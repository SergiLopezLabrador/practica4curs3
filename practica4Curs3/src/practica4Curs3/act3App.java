package practica4Curs3;

public class act3App {

	public static void main(String[] args) {
		
		//Aqu� crear� variables
		
		int X = 6;
		int Y = 8;
		double N = 3.6;
		double M = 1.9;
		
		//Aqu� mostro totes les operacions amb les variables que hem creat anteriorment
		System.out.println("Variable X: " + X);
		System.out.println("Variable Y: " + Y);
		System.out.println("Variable N: " + N);
		System.out.println("Variable M: " + M);
		System.out.println("La suma de X + Y = " + (X + Y));
		System.out.println("La diferencia X - Y = " + (X - Y));
		System.out.println("El producto X * Y = " + (X * Y));
		System.out.println("El cociente X / Y = " + (X / Y));
		System.out.println("El resto X % Y = " + (X % Y));
		System.out.println("La suma N + M = " + (N + M));
		System.out.println("La diferencia N - M = " + (N - M));
		System.out.println("El producto N * M = " + (N * M));
		System.out.println("El cociente N / M = " + (N / M));
		System.out.println("El resto N % M = " + (N % M));
		System.out.println("La suma X + N = " + (X + N));
		System.out.println("El cociente Y / M = " + (Y / M));
		System.out.println("El resto Y % M = " + (Y % M));
		System.out.println("El doble de la variable X: " + X*2);
		System.out.println("El doble de la variable Y: " + Y*2);
		System.out.println("El doble de la variable N: " + N*2);
		System.out.println("El doble de la variable M: " + M*2);
		//Aqu� realitzarem les ultimes operacions
		System.out.println("La suma de todas las variables (X + Y + N + M) = " + (X+Y+N+M));
		System.out.println("La multiplicaci�n de todas las variables (X * Y * N * M) = " + (X * Y * N * M));
		

	}

}
